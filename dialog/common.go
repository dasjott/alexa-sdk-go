package dialog

// Objects common to request and response

// EchoIntent is the json part for an intent
type EchoIntent struct {
	Name               string              `json:"name"`
	Slots              map[string]EchoSlot `json:"slots"`
	ConfirmationStatus string              `json:"confirmationStatus,omitempty"`
}
